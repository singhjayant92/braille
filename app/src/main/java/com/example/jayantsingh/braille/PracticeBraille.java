package com.example.jayantsingh.braille;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class PracticeBraille extends AppCompatActivity {

    boolean brailleLetter[] = {false, false, false, false, false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice_braille);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

/*        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void dotToggle(View view){
        int changedBraille = Integer.parseInt(view.getTag().toString());
        brailleLetter[changedBraille] = !brailleLetter[changedBraille];
        if(brailleLetter[changedBraille] == false){
            ((ImageView) view).setImageResource(R.drawable.dot_whitew);
        }else{
            ((ImageView) view).setImageResource(R.drawable.dot_blackw);
        }

        ((TextView)findViewById(R.id.englishLetter)).setText(brailleToChar());
    }

    protected String brailleToChar(){
        char character = '`';
        if(brailleLetter[0] && !brailleLetter[1] && !brailleLetter[3] && !brailleLetter[4])
            character = 'A';
        else if(brailleLetter[0] && brailleLetter[1] && !brailleLetter[3] && !brailleLetter[4])
            character = 'B';
        else if(brailleLetter[0] && !brailleLetter[1] && brailleLetter[3] && !brailleLetter[4])
            character = 'C';
        else if(brailleLetter[0] && !brailleLetter[1] && brailleLetter[3] && brailleLetter[4])
            character = 'D';
        else if(brailleLetter[0] && !brailleLetter[1] && !brailleLetter[3] && brailleLetter[4])
            character = 'E';
        else if(brailleLetter[0] && brailleLetter[1] && brailleLetter[3] && !brailleLetter[4])
            character = 'F';
        else if(brailleLetter[0] && brailleLetter[1] && brailleLetter[3] && brailleLetter[4])
            character = 'G';
        else if(brailleLetter[0] && brailleLetter[1] && !brailleLetter[3] && brailleLetter[4])
            character = 'H';
        else if(!brailleLetter[0] && brailleLetter[1] && brailleLetter[3] && !brailleLetter[4])
            character = 'I';
        else if(!brailleLetter[0] && brailleLetter[1] && brailleLetter[3] && brailleLetter[4])
            character = 'J';
        else if(!brailleLetter[0] && !brailleLetter[1] && !brailleLetter[3] && !brailleLetter[4] && !brailleLetter[2] && !brailleLetter[5])
            return "Enter a Braille Character";
        else{
//            Log.i("PracticeBraille", "char: " + character + " value: " + (int) character);
            return "Invalid Character";
        }

        if(brailleLetter[2] || brailleLetter[5]) {
            if (brailleLetter[2] && !brailleLetter[5])
                character = (char) ((int) character + 10);

            else if (brailleLetter[2] && brailleLetter[5]) {
                if (((int) character) >= ((int) 'F')) {
//                    Log.i("PracticeBraille", "char: " + character + " value: " + (int) character);
                    return "Invalid Character";
                }
                else if (((int) character) >= ((int) 'C'))
                    character = (char) ((int) character + 21);
                else
                    character = (char) ((int) character + 20);

            }else if (character == 'J' && !brailleLetter[2] && brailleLetter[5])
                character = 'W';
            else {
//                Log.i("PracticeBraille", "char: " + character + " value: " + (int) character);
                return "Invalid Character";
            }
        }

        if(character == '`') {
//            Log.i("PracticeBraille", "char: " + character + " value: " + (int) character);
            return "Invalid Character";
        }
//        Log.i("PracticeBraille", "char: " + character + " value: " + (int) character);
        return ""+character;
    }
}
